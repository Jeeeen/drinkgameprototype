﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class PointsValues : MonoBehaviour 
{
	[Header("Point Values ")]
	public int MixingPoints ;
	public int ButtonPressPoints;
	public int CatchingPoints;
	public int ServingPoints;
	public int TotalWholePointsMultiplier;
}
