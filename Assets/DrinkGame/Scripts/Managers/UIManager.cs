﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class UIManager : MonoBehaviour 
{
	public Text CurrentBeat;
	public Text BeatSuccesText;
	public Text ScoreText;
    public Text CoinsText;
	private UI_Controller ui_controller; 
	public Text ComboMultiplierText;
	public Text PlusPointsText;

	void OnEnable()
	{
		GameManager.BeatSuccessEvent += OnBeatSuccessEvent;
		GameManager.EveryBeatEvent += OnEveryBeatEvent;
        GameManager.MilkPopCapEvent += OnMilkPopCapEvent;
	}

	void OnDisable()
	{
		GameManager.EveryBeatEvent -= OnEveryBeatEvent;
		GameManager.BeatSuccessEvent -= OnBeatSuccessEvent;
        GameManager.MilkPopCapEvent -= OnMilkPopCapEvent;

	}

	public void Init()
	{
		ui_controller = (UI_Controller)FindObjectOfType(typeof (UI_Controller));
		CurrentBeat = ui_controller.CurrentBeat;
		BeatSuccesText = ui_controller.BeatSuccesText;
		ScoreText = ui_controller.ScoreText;
		ComboMultiplierText = ui_controller.ComboMultiplierText;
        CoinsText = ui_controller.CoinRewardsText;
	}

    #region Event Handlers

    private void OnBeatSuccessEvent (bool success,int points,MixedBeat mixBeat)
	{
		ShowBeatSuccesText (success);
	}

    private void OnEveryBeatEvent(int currentBeat)
    {
        ShowCurrentBeatText (currentBeat);
    }

    private void OnMilkPopCapEvent(double curDoubleBeatNumber)
    {
        if (GameManager.I._soundManager.GetOnBeatHitResult() == OnBeatHitResult.OnBeat)
        {
            ShowBeatSuccesText(true);
        }
        else 
        {
            ShowBeatSuccesText(false);
        }
    }
    #endregion Event Handlers



	void ShowCurrentBeatText(int currentBeat)
	{
		CurrentBeat.text = "Current Beat : " +  currentBeat.ToString();
	}

	void ShowPlusPointsText(int points)
	{
		PlusPointsText.text = " + " + points.ToString ();
		PlusPointsText.color = Color.green;
		StartCoroutine (ClearTextAfterBeat (ClearlusPointsText));
	}

	void ClearlusPointsText()
	{
		PlusPointsText.text = "";
	}

	public void ShowBeatSuccesText(bool success)
	{
		if (success) 
		{
			BeatSuccesText.text = "ON BEAT";
			BeatSuccesText.color = Color.green;
		} 
		else 
		{
			BeatSuccesText.text = "OFF BEAT";
			BeatSuccesText.color = Color.red;
		}

		StartCoroutine (ClearTextAfterBeat (ClearBeatSuccesText));
		UpdateScoreText (GameManager.I._scoreManager.Scores);
	}

	public void UpdateScoreText(int Score)
	{
		ScoreText.text  = "Score : " + Score.ToString();
	}

    public void UpdateCoinsText(int coins)
    {
        CoinsText.text  = "Coins Reward  : " + coins.ToString();
    }

	public void UpdateComboText(int comboMultiplier)
	{
		if (comboMultiplier == 1) 
		{
			ComboMultiplierText.text  = "";
			return;
		}
		ComboMultiplierText.text  = "Combo X " + comboMultiplier.ToString();
	}

	public void ClearBeatSuccesText()
	{
		BeatSuccesText.text = "";
	}

	IEnumerator ClearTextAfterBeat(Action clearTextMethod)
	{
		yield return new WaitForSeconds (GameManager.I._soundManager.beatTime);
		clearTextMethod.Invoke ();
	}
}
