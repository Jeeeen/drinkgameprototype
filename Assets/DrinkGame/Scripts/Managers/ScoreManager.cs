﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour 
{
	public int Scores;
    public int CoinsRewarded;
    private int requestCatId;
 	private int ComboMultiplier = 1;
	public List<int> ComboBeats;
	private MixedBeat currentComboBeat;
	public List<TotalPointsCheck> _totPointsCheck = new List<TotalPointsCheck>();
    public UIManager ui_manager { get { return GameManager.I._uiManager; }}


	void OnEnable()
	{
		GameManager.BeatSuccessEvent += OnBeatSuccessEvent;
		GameManager.EveryBeatEvent +=  OnEveryBeatEvent;;
		GameManager.ServeMixEvent += OnServeMixEvent;
	}

   
    void OnDisable()
    {
        GameManager.BeatSuccessEvent -= OnBeatSuccessEvent;
        GameManager.EveryBeatEvent -=  OnEveryBeatEvent;
        GameManager.ServeMixEvent -= OnServeMixEvent;
    }

	void OnServeMixEvent (int catId)
	{
		Debug.Log("OnServeMixEvent");

		var finishedCheck = _totPointsCheck.Find( x=> x.catId == catId);
		finishedCheck.realBeat = GameManager.I._soundManager._currentIntBeatNumber;
		GetTotalPoints(finishedCheck);
	}

   

	void Update()
	{
//		if(Input.GetKeyDown(KeyCode.Space))			
//			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.MixingPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.Mix));
//
//		if(Input.GetKeyDown(KeyCode.KeypadEnter))			
//			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.ButtonPressPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.BtnPress));
//
//		if(Input.GetKeyDown(KeyCode.Keypad0))			
//			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.ButtonPressPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.BtnCatch));
	}

	void OnEveryBeatEvent (int currentBeatNumber)
	{
		StartCoroutine(CheckComboEveryBeat(currentBeatNumber));
	}

	public IEnumerator CheckComboEveryBeat(int currentBeatNumber)
	{
		yield return new WaitForSeconds(GameManager.I._soundManager.beatTime);
		if (currentComboBeat!=null && currentComboBeat.beatNumber != currentBeatNumber && currentComboBeat.mixAction != MixAction.BtnPress) 
		{
			Debug.Log ("OnEveryBeatEvent" + "FAIL COMBO");
			ClearCombo ();
		}

		if (currentComboBeat!=null && currentBeatNumber - currentComboBeat.beatNumber >= 4 && currentComboBeat.mixAction == MixAction.BtnPress) 
		{
			Debug.Log ("OnEveryBeatEvent" + "FAIL COMBO");
			ClearCombo ();
		}
	}

	

	private void OnBeatSuccessEvent(bool success, int points, MixedBeat mixBeat)
	{
		if (!success) 
			ClearCombo ();
		else 
			UpdatePlayerScore (points,mixBeat);
	}



	public void UpdatePlayerScore(RequestResult result)
	{
		if (result == RequestResult.Right) 
		{
			Scores += 20;
			GameManager.I._uiManager.UpdateScoreText (Scores);
			GameManager.I.raiseBeatSuccessEvent (true, new MixedBeat());
		}
        else if(GameManager.I.GAME_MODE != GameMode.Reward)
		{
			GameManager.I.raiseBeatSuccessEvent (false, new MixedBeat());
		}
	}

    public void UpdatePlayerCoins(RequestResult result ,int rewardCoins, OnBeatHitResult onBeatHitResult)
    {   

        Debug.Log("updating Player coins result =" + result.ToString());



        if (result == RequestResult.Right )
        {
            ui_manager.ShowBeatSuccesText(true);
            CoinsRewarded += rewardCoins;
            ui_manager.UpdateCoinsText(CoinsRewarded);
        }

        if (onBeatHitResult == OnBeatHitResult.OffBeat && result!= RequestResult.WrongDrink)
        {
            ui_manager.ShowBeatSuccesText(false);
        }
    }

	public void UpdatePlayerScore(int points,MixedBeat mixedBeat)
	{
		CheckCombo (mixedBeat);

		int plusPoints = points * ComboMultiplier;

		Scores += plusPoints;

		GameManager.I._uiManager.UpdateScoreText (Scores);
	}

	public void CheckCombo(MixedBeat currentMixBeat)
	{
		bool contains = false;

		if (currentMixBeat.mixAction == MixAction.Mix || currentMixBeat.mixAction == MixAction.BtnPress || currentMixBeat.mixAction == MixAction.BtnServe)
		{
			contains = ComboBeats.Contains (currentMixBeat.beatNumber - 1);
		}

		if (currentMixBeat.mixAction == MixAction.BtnCatch)
		{
			contains = ComboBeats.Contains (currentMixBeat.beatNumber - 4);
			if(contains)
				Debug.Log("contains");
		}

		if (contains) 
		{
			currentComboBeat = currentMixBeat;
			ComboMultiplier =  ComboMultiplier  + 1;
			GameManager.I._uiManager.UpdateComboText (ComboMultiplier);
			ComboBeats.Add (currentMixBeat.beatNumber);
			Debug.Log ("Combo is going on. multilplier =  " + ComboMultiplier);
		}
		else
		{
			currentComboBeat = currentMixBeat;
			Debug.Log ("Combo is failed  on " + ComboMultiplier);
			ClearCombo ();
			ComboBeats.Add (currentMixBeat.beatNumber);
		}
 	}

	private void ClearCombo()
	{
		if(GameManager.I._uiManager.ComboMultiplierText == null)
			return;
		ComboMultiplier = 1;
		GameManager.I._uiManager.UpdateComboText (ComboMultiplier);
		ComboBeats.Clear ();
	}

	public void  StartTotalPointsCalculate(int catId)
	{
		var totPointsCheck = new TotalPointsCheck(GameManager.I._soundManager._currentIntBeatNumber, catId);
		_totPointsCheck.Add(totPointsCheck);
	}

	private void GetTotalPoints(TotalPointsCheck check)
	{
		if(check.realBeat > check.maxBeat)
		{
			Debug.Log("Too Late. 0 Total Points ");
			return;
		}
		int TotPoints = (check.maxBeat  - check.realBeat) * GameManager.I._pointValues.TotalWholePointsMultiplier;
		Scores += TotPoints;
		GameManager.I._uiManager.UpdateScoreText (Scores);
		Debug.Log("[ScoreManger] Get Total Points = " + TotPoints);
	}

}

public class TotalPointsCheck
{
	public int startBeat;
	public int maxBeat;
	public int realBeat;
	public int catId;

	public TotalPointsCheck(int startBeat, int CatId)
	{
		this.startBeat = startBeat;
		this.catId = CatId;
		maxBeat = 30 + startBeat;
	}
}
