﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour 
{


    #region delegates
    public delegate  void PuddingServedDel(int beatNumber);
    public delegate  void EveryBeatDel(int curBeatNumber);
    public delegate  void EveryHalfBeatDel (float curFloatBeatNumber);
    public delegate  void MilkPopCapDel (double curDoubleBeatNumber);
    public delegate  void TeethHitDel (int TeethId);
    public delegate  void BeatSuccessDel (bool success,int beatNumber, MixedBeat mixedBeat) ;
    public delegate  void ServedDel(RequestType drinktype, int beatNumber, int catId);
    public delegate  void MixedDrinkServeDel (MixedDrink type ,int currentBeat,MixTriggerSide side);
    public delegate  void MixFinishDel (bool success,MixTriggerSide side) ;
    public delegate  void CheckOnBeatMixDel (int points, MixedBeat mixBeat);
    public delegate  void ServeMixDel(int catId);
    public delegate  void ServeDel (RequestType drinktype, int beatNumber, int catId, int rewardCoins, OnBeatHitResult onBeatHitResult);
    #endregion 

	#region events
    /// <summary>
    /// Occurs when pudding drink is served.
    /// </summary>
    public static event PuddingServedDel PuddingServedEvent;

    /// <summary>
    /// Occurs on every music beat.
    /// </summary>
    public static event EveryBeatDel EveryBeatEvent;

    /// <summary>
    /// Occurs on every half music beat.
    /// </summary>
    public static event EveryHalfBeatDel EveryHalfBeatEvent;

    /// <summary>
    /// Occurs when milk cap had been poped.
    /// </summary>
    public static event MilkPopCapDel MilkPopCapEvent;

    /// <summary>
    /// Occurs when player hit a teeth.
    /// </summary>
    public static event TeethHitDel TeethHitEvent;

    /// <summary>
    /// Occurs when some of drink is served.
    /// </summary>
    public static event BeatSuccessDel BeatSuccessEvent;

    /// <summary>
    /// Occurs when served event.
    /// </summary>
    public static event ServedDel ServedEvent; 

    /// <summary>
    /// Occurs when drink is served to early.
    /// </summary>
	public static event Action OnEarlySongEvent; 

    /// <summary>
    /// Occurs when mixed drink is served.
    /// </summary>
    public static event MixedDrinkServeDel MixedDrinkServeEvent;
    /// <summary>
    /// Occurs when mix proccess is finished.
    /// </summary>
    public static event MixFinishDel MixFinishEvent;

    /// <summary>
    /// Check if it's beat hitting hit the "OnBeat" Situation.
    /// </summary>
    public static event CheckOnBeatMixDel CheckOnBeatMixEvent;

    /// <summary>
    /// Occurs when mixed button is served to the cat.
    /// </summary>
	public static event Action<int> ServeMixEvent; // <catId>

    /// <summary>
    /// Occurs when drink is served in reward mode.
    /// </summary>
    public static event ServeDel ServedRewardEvent;

	#endregion events

	public GameMode GAME_MODE;
	public CatController[] _catsControllers;
	public TeethController[] _teethControllers;
	public List<Request> requestAll;
	public Transform[] SpawnPos;
	public static GameManager I;

	#region Managers
	public SoundManager _soundManager;
	public UIManager _uiManager;
	public ScoreManager _scoreManager;
	public SongManager _songManager;
	public PointsValues _pointValues;
	#endregion 

	void Start()
	{
		if (I == null) 
		{
			I = this;
		}

		InitGameManager();
	}

	void InitAllRequests()
	{
		_songManager.GameSongs[0].GetAllSongRequest ();
	}

	void InitGameManager()
	{
		InitAllManagers ();
		InitAllCats ();
		InitAllTeeths ();
	}

	void InitAllManagers()
	{
		GetSongManager().Init ();
		_soundManager.Init ();
		_uiManager.Init();
	}

	void InitAllCats()
	{
		if(_catsControllers.Length < 4)
		{
			_catsControllers = (CatController[])FindObjectsOfType(typeof(CatController));
			Array.Sort(_teethControllers,((x, y) => x.TeethId - y.TeethId));
		}
		for (int i = 0; i < _catsControllers.Length; i++) 
		{
			_catsControllers [i].Init ();
		}
	}

	void InitAllTeeths()
	{
		if(_teethControllers.Length < 5)
		{
			_teethControllers = (TeethController[])FindObjectsOfType(typeof(TeethController));
			Array.Sort(_teethControllers,((x, y) => x.TeethId - y.TeethId));
		}

		for (int i = 0; i < _teethControllers.Length; i++) 
		{
			_teethControllers [i].Init ();
		}
	}

	public void StartRequest(Request request, int catId)
	{
		_soundManager.PlayRequestSound(request,catId);
		_uiManager.ClearBeatSuccesText ();
		_scoreManager.StartTotalPointsCalculate(catId);
	}

  
	private SongManager GetSongManager()
	{
		if(_songManager != null)
			return _songManager;
		else
		{
			_songManager = FindObjectOfType<SongManager>();
			return _songManager;
		}
	}

	public void RestartSong()
	{
		InitGameManager();
		RestartPosAllCats();
	}

	public void RestartPosAllCats()
	{
		for (int i = 0; i < _catsControllers.Length; i++) 
		{
			_catsControllers [i].RestartCat ();
		}   
	}


	#region raising Events


	public void raisePuddingServedEvent()
	{
		if (PuddingServedEvent != null) 
		{
			PuddingServedEvent (_soundManager._currentIntBeatNumber);
		}
	}

    public void raiseServeRewardEvent(RequestType drinktype, int beatNumber, int catId, int rewardCoins,OnBeatHitResult onBeatHitResult)
    {
        if (ServedRewardEvent != null) 
        {
            ServedRewardEvent (drinktype,beatNumber,catId,rewardCoins,onBeatHitResult);
        }
    }

	public void raiseBeatEvent(int beatnumber)
	{
        if (EveryBeatEvent != null)
        {
            EveryBeatEvent (beatnumber);
        }
	}

	public void raiseOnServedEvent(RequestType drinktype,int beatnumber,int CatId)
	{
		if (ServedEvent != null) 
		{
			ServedEvent (drinktype, beatnumber, CatId);
		}
	}

	public void raiseBeatSuccessEvent(bool success,MixedBeat mixBeat,int points = 0)
	{
		if(BeatSuccessEvent!=null)
		{
			BeatSuccessEvent (success,points,mixBeat);
 		}
	}

	public void raiseOnMilkPopCapEvent(double beatNumber)
	{
		if(MilkPopCapEvent!=null)
		{
			MilkPopCapEvent (beatNumber);
		}
	}

	public void raiseOnEarlyServedEvent()
	{
		if(OnEarlySongEvent!=null)
		{
			OnEarlySongEvent();
		}
	}

	public void raiseEveryHalfBeatEvent(float beatNumber)
	{
		if(EveryHalfBeatEvent!=null)
		{
			EveryHalfBeatEvent(beatNumber);
		}
	}

	public void raiseTeethHitEvent(int teethID)
	{
		if(TeethHitEvent!=null)
		{
			TeethHitEvent(teethID);
		}
	}

	public void raiseMixDrinkServeEvent(MixedDrink type ,int currentBeat,MixTriggerSide side)
	{
		if ( MixedDrinkServeEvent!= null) 
		{
			MixedDrinkServeEvent (type,currentBeat,side);
		}
	}

	public void raiseMixFinishEvent(bool success,MixTriggerSide side)
	{
		if ( MixFinishEvent!= null) 
		{
			MixFinishEvent (success,side);
		}
	}

	public void raiseCheckOnBeatMixEvent(int points, MixedBeat mixBeat)
	{
		if ( CheckOnBeatMixEvent!= null) 
		{
			CheckOnBeatMixEvent (points,mixBeat);
		}
	}

	public void raiseServeMixEvent(int catID)
	{
		if ( ServeMixEvent!= null) 
		{
			ServeMixEvent (catID);
		}
	}

	#endregion 

}
