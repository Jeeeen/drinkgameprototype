﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongManager : MonoBehaviour
{
	[HideInInspector]
	public List<Song> GameSongs;

	[Header("Drag here background Song.cs script from one of the Child Object")]
	[Tooltip("Drag here background Song.cs script from one of the Child Object")]
	public Song CurrentBgSong;

	public BeatSystem beatSystemType;

	private AudioClip CurrentSongAudio;


	public void Init()
	{
		for (int i = 0; i < transform.childCount; i++) 
		{
			GameSongs.Add (transform.GetChild (i).GetComponent<Song>());
		}
	}

	public AudioClip GetCurrentSongAudio()
	{
		return	CurrentBgSong.AudioClip;
	}
}
