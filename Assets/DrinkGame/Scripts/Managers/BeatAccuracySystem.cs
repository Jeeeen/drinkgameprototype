﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatAccuracySystem : MonoBehaviour 
{
	
	[Header("Type here the On Beat Range from Start Point to End Point")]
	[Tooltip("Start point of hitting beat of previous beat")]
	public double StartPoint;
	[Tooltip("Finish point of hitting beat of previous beat")]
	public double FinishPoint;
}
