﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class SoundManager : MonoBehaviour 
{
	public AudioSource RequestAudioSource;
	public AudioSource ObjectAudioSource;
	public AudioSource MixedAudioSource;
	private AudioSource BGaudioSource;
	public List<Request> requestAll;

	[HideInInspector]
	public float beatTime;
	[HideInInspector]
	public int _currentIntBeatNumber;
	[HideInInspector]
	public double _currentDoubleBeatNumber;

	private BeatSystem bsType;
	private int BeatTimer;
	private float timer;
	private int _lastBeatNumber;
	private float _servedBeatNumber;
	public BeatAccuracySystem _beatAccuracySystem;

    [Range(0f,0.5f)]
    [TooltipAttribute("Play the coin sounds  earlier on this offset value in seconds  (0.5s - beat time, 0 - no offset")]
    public float coinSoundOffset;

	#region AudioClips

	public AudioClip[] requestClips = new AudioClip[3];
	public AudioClip[] servedClips = new AudioClip[3];
	public AudioClip[] teethClips = new AudioClip[5];
	public AudioClip[] OneShotClips;

	public AudioClip[] Cat0_RequestClips = new AudioClip[4];
	public AudioClip[] Cat1_RequestClips = new AudioClip[4];
	public AudioClip[] Cat2_RequestClips = new AudioClip[4];
	public AudioClip[] Cat3_RequestClips = new AudioClip[4];
	public AudioClip[][] CatsRequestClips = new AudioClip[4][];

	private AudioClip[] MixedDrinksClips = new AudioClip[6];


	[Header("Mixed_Drink_Clips")]

	[Tooltip("Orange")]
	public AudioClip OrangeClip;
	[Tooltip("Green")]
	public AudioClip GreenClip;
	[Tooltip("Yellow")]
	public AudioClip YellowClip;
	[Tooltip("Pink")]
	public AudioClip PinkClip;
	[Tooltip("Cyan")]
	public AudioClip CyanClip;
	[Tooltip("Blue")]
	public AudioClip BlueClip;

	[Header("Speech Bubble Clip")]
	public AudioClip SpeechBubbleClip;

	[Header("Blend Sound Clip")]
	public AudioClip BlendClip;


	[Header("Fly Sound Clip")]
	public AudioClip FlyClip;

	[Header("Milk Pop Cap Clip")]
	public AudioClip MilkPopCapClip;


    [Header("Request Clips For RewardMode")]
    public AudioClip MilkRequestRewardClip;
    public AudioClip PuddingRequestRewardClip;
    public AudioClip CoinDissapearClip;



	#endregion


	public void Init () 
	{
		BGaudioSource = GetComponent<AudioSource> ();
		InitBackGroundMusic ();
		InitBeatDetectSystem ();
		InitCatsRequestClips ();
		InitMixedDrinkClips ();

	}

	private void OnEnable()
	{
		GameManager.ServedEvent += OnServedEvent;
		GameManager.OnEarlySongEvent += OnEarlyServedEvent;
		GameManager.MilkPopCapEvent +=  OnMilkPopCapEvent;
		GameManager.CheckOnBeatMixEvent += OnCheckOnBeatMixEvent;
		GameManager.ServedRewardEvent +=  OnServedRewardEvent;;
	}

	private void OnDisable()
	{
		GameManager.ServedEvent -= OnServedEvent;
		GameManager.OnEarlySongEvent -= OnEarlyServedEvent;
		GameManager.MilkPopCapEvent -=  OnMilkPopCapEvent;
		GameManager.ServedRewardEvent -=  OnServedRewardEvent;;
		GameManager.CheckOnBeatMixEvent -= OnCheckOnBeatMixEvent;
	}

	void OnServedRewardEvent (RequestType drinktype, int beatNumber, int catId, int rewardCoins, OnBeatHitResult onBeatHitResult)
	{
		PlayServeSound (drinktype);
	}

	void OnCheckOnBeatMixEvent (int possiblePoints, MixedBeat mixBeat)
	{
		int d = (int)_currentDoubleBeatNumber;

		double fract = Math.Round(_currentDoubleBeatNumber,1) - d;

		if (fract <= _beatAccuracySystem.FinishPoint || fract >= _beatAccuracySystem.StartPoint)
		{			
			Debug.Log("On beat hit point  = " + fract);
			GameManager.I.raiseBeatSuccessEvent (true,mixBeat,possiblePoints);
		}
		else
		{
			Debug.Log("Off beat hit point = " + fract);
			GameManager.I.raiseBeatSuccessEvent (false,mixBeat,possiblePoints);
		}
	}

    public OnBeatHitResult GetOnBeatHitResult ()
    {
        int d = (int)_currentDoubleBeatNumber;

        double fract = Math.Round(_currentDoubleBeatNumber,1) - d;

		if (fract <= _beatAccuracySystem.FinishPoint || fract >= _beatAccuracySystem.StartPoint)
		{			
			Debug.Log("On beat hit point  = " + fract);
			return OnBeatHitResult.OnBeat;
		}

		else
		{
			Debug.Log("Off beat hit point = " + fract);
			return OnBeatHitResult.OffBeat;
		}
    }

	private void Update () 
	{
		DetectBeatsOldSystem ();
	}

	public void InitBeatDetectSystem()
	{
		switch (GameManager.I._songManager.beatSystemType) 
		{
		case BeatSystem.New:
			
			_currentDoubleBeatNumber = 0.5d;
			InvokeRepeating ("ChangeHalfBeat", 0,beatTime/10f);
			InvokeRepeating ("ChangeBeat", 0, beatTime);
			InvokeRepeating ("CheckingBeat", 0, 0.01f);
			bsType = BeatSystem.New;
			break;
		case BeatSystem.Old:
			bsType = BeatSystem.Old;
			break;
		}
	}

	public void InitCatsRequestClips()
	{
		CatsRequestClips [0] = Cat0_RequestClips;
		CatsRequestClips [1] = Cat1_RequestClips;
		CatsRequestClips [2] = Cat2_RequestClips;
		CatsRequestClips [3] = Cat3_RequestClips;
	}

	public void InitMixedDrinkClips()
	{
		MixedDrinksClips [0] = OrangeClip;
		MixedDrinksClips [1] = GreenClip;
		MixedDrinksClips [2] = YellowClip;
		MixedDrinksClips [3] = PinkClip;
		MixedDrinksClips [4] = CyanClip;
		MixedDrinksClips [5] = BlueClip;
	}

	private  bool BeatIsChanged()
	{
		if (_lastBeatNumber != _currentIntBeatNumber)
		{
			_lastBeatNumber = _currentIntBeatNumber;
			return true;
		}
		return false;
	}

	public void DetectBeatsOldSystem()
	{
		if (bsType != BeatSystem.Old)
			return;
		
		_currentIntBeatNumber = (int)(BGaudioSource.time / beatTime);

		if (BeatIsChanged ()) 
		{
			GameManager.I.raiseBeatEvent (_currentIntBeatNumber);
			StartCoroutine(raiseHalfBeatEvent (_currentIntBeatNumber,beatTime));

			if(_currentIntBeatNumber == 0)
			{
				GameManager.I.RestartPosAllCats();
			}
		}
	}

	private bool CheckIsRestartSong()
	{
		return(BGaudioSource.time / beatTime) == 0;
	}

	private void OnMilkPopCapEvent (double currentBeat)
	{
		PlayMilkPopCapSound ();
	}

	private void OnServedEvent (RequestType drinktype, int beatNumber, int catIds)
	{
		PlayServeSound (drinktype);
	}

	private void OnEarlyServedEvent ()
	{
		RequestAudioSource.Stop();
	}

	public void PlayRequestSound(Request request,int catId)
	{
//		if(RequestAudioSource.isPlaying)
//		{
//			var tempAS = gameObject.AddComponent<AudioSource>();
//			tempAS.clip = requestClips[(int)request.requestType];
//			tempAS.Play();
//		}


        if (GameManager.I.GAME_MODE != GameMode.Reward)
        {
            RequestAudioSource.clip = CatsRequestClips[catId][(int)request.requestType];
            RequestAudioSource.Play();
        }
        else if(GameManager.I.GAME_MODE == GameMode.Reward)
        {
            if (request.requestType == RequestType.Pudding)
            {
                RequestAudioSource.clip = PuddingRequestRewardClip;
                RequestAudioSource.Play();
            }
            if (request.requestType == RequestType.Milk)
            {
                RequestAudioSource.clip = MilkRequestRewardClip;
                RequestAudioSource.Play();
            }
        }
	}

	public void PlayServeSound(RequestType serveType)
	{
		ObjectAudioSource.PlayOneShot (servedClips [(int)serveType]);
	}

	public void PlayTeethSound(int teethID)
	{
		BGaudioSource.PlayOneShot (teethClips[teethID]);
	}

	public void PlayMixedDrinkSound(MixedDrink mixDrinkType)
	{
		MixedAudioSource.PlayOneShot (MixedDrinksClips[(int)mixDrinkType]);
	}

//	public void PlaySpeechBubbleSound()
//	{
//		RequestAudioSource.PlayOneShot (SpeechBubbleClip);
//	}

	public void PlayBlendSound()
	{
		ObjectAudioSource.PlayOneShot (BlendClip,20f);
	}

	public void PlayFlySound()
	{
		RequestAudioSource.PlayOneShot (FlyClip);
	}


	public void PlayMilkPopCapSound()
	{
		RequestAudioSource.PlayOneShot (MilkPopCapClip);
	}

    public void PlayCoinDissapearSound()
    {
        ObjectAudioSource.PlayOneShot (CoinDissapearClip);
    }


	private void InitBackGroundMusic()
	{
		BGaudioSource.clip = GameManager.I._songManager.CurrentBgSong.AudioClip;
		beatTime = 60 / GameManager.I._songManager.CurrentBgSong.BPM;
		BGaudioSource.Play();
	}

	IEnumerator raiseHalfBeatEvent( int currentBeat,float beatTime)
	{
		yield return new WaitForSeconds (beatTime / 2);
		_currentDoubleBeatNumber = currentBeat + 0.5f;
		GameManager.I.raiseEveryHalfBeatEvent (currentBeat + 0.5f);
	}


	private void ChangeBeat()
	{
		_currentIntBeatNumber++;

		GameManager.I.raiseBeatEvent (_currentIntBeatNumber);
		
		if(_currentIntBeatNumber == 0)
    	{
    		GameManager.I.RestartPosAllCats();
    	}
	}

	public bool IsOnBeat()
	{
		int d = (int)_currentDoubleBeatNumber;

		double fract = Math.Round(_currentDoubleBeatNumber,1) - d;

		if (fract == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void PlayObjectImpactSound()
	{
		if (ObjectAudioSource.isPlaying )
			return;
        
		ObjectAudioSource.PlayOneShot (OneShotClips[(int) Sounds.Collision],20f);
	}


	public void TestBeatSucces()
	{
		int d = (int)_currentDoubleBeatNumber;

		double fract = Math.Round(_currentDoubleBeatNumber,1) - d;

		if (Input.GetKeyDown (KeyCode.KeypadEnter) && fract == 0) {
			Debug.Log ("WHOLE BEAT");
		}

	}

	public void ChangeHalfBeat()
	{	
		_currentDoubleBeatNumber = _currentDoubleBeatNumber + 0.1f;
	}


	public void CheckingBeat()
	{

		int d = (int)_currentDoubleBeatNumber;

		double fract = Math.Round(_currentDoubleBeatNumber,1) - d;

		if (Input.GetKeyDown (KeyCode.KeypadEnter) && fract == 0)
		{
			Debug.Log ("On Beat");
		}
	}



}
