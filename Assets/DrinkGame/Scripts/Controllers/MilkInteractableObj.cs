﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MilkInteractableObj : VRTK_InteractableObject
{

	public GameObject water;

	public override void Grabbed (GameObject currentGrabbingObject)
	{

		if (gameObject.GetComponent<Rigidbody> () == null) 
		{
			gameObject.AddComponent<Rigidbody> ();
		}
		else
		{
			if(!gameObject.GetComponent<Rigidbody> ().useGravity)
			{
				gameObject.GetComponent<Rigidbody> ().useGravity = true;
			}        
		}

        if (gameObject.GetComponent<Collider>() != null)
        {
            if (gameObject.GetComponent<Collider> ().isTrigger)
            {
                gameObject.GetComponent<Collider> ().isTrigger = false;
            }
        }

     
		if(water != null)
		{
			water.SetActive(true);
		}

		base.Grabbed (currentGrabbingObject);
	}

	public override void Ungrabbed(GameObject previousGrabbingObject)
	{   
		if(water != null)
		{
			water.SetActive(false);
		}

		base.Ungrabbed(previousGrabbingObject);
	}
}
