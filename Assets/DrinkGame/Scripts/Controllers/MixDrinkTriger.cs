﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MixDrinkTriger : MonoBehaviour {

	public Vector3 ServePosition = new Vector3(-0.3f,0.085f,0.005f);
	private Vector3 buttonSpawnPos ;
	private Quaternion buttonSpawnRot ;
	public MixedButtonController Button;
	public MixTriggerSide Side;


	void OnEnable()
	{
		buttonSpawnPos = transform.GetChild(0).transform.position;
		buttonSpawnRot = transform.GetChild(0).transform.rotation;
		GameManager.MixFinishEvent += OnMixFinishEvent;
	}

	void OnMixFinishEvent (bool success,MixTriggerSide side)
	{
		if (success && Side == side) 
		{
			if(Button!=null)
			Button.SetButtonPhysic (true);
		}
	}

	void OnDisable()
	{
		GameManager.MixFinishEvent -= OnMixFinishEvent;
	}


	public void SpawnMixButton()
	{
		var drinkButtonPrefab = (Side == MixTriggerSide.Left)?Resources.Load (Constants.LeftDrinkButtonPrefabPath) as GameObject :Resources.Load (Constants.RightDrinkButtonPrefabPath) as GameObject;
		GameObject button = Instantiate (drinkButtonPrefab, this.transform);
		button.transform.position = buttonSpawnPos;
		button.transform.rotation = buttonSpawnRot;

		Button = button.GetComponent<MixedButtonController>();
	}

	private void SpawnMixDrink(GameObject drinkPrefab)
	{
		GameObject drink = Instantiate (drinkPrefab, this.transform);
		drink.transform.localPosition = ServePosition;
		Destroy (drink, GameManager.I._soundManager.beatTime);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.MixedDrinkTag)) 
		{
			if (other.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject () != null)
			{
				GameObject grabbingObj = other.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ();
				var grabController = other.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ().GetComponent<VRTK_ControllerActions>();
				grabController.TriggerHapticPulse (1,0.25f,0.01f);
				grabbingObj.GetComponent<VRTK_InteractGrab>().ForceRelease ();
			}

			GameManager.I._soundManager.PlayMixedDrinkSound(other.gameObject.GetComponent<MixDrinkController> ().mixedDrinkType);

			GameManager.I.raiseMixDrinkServeEvent (other.gameObject.GetComponent<MixDrinkController> ().mixedDrinkType,GameManager.I._soundManager._currentIntBeatNumber,Side);

			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.MixingPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.Mix));
		
			Destroy (other.gameObject);
		} 
	}

}
