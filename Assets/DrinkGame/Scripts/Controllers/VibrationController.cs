﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class VibrationController : MonoBehaviour 
{
	public SteamVR_TrackedObject associatedTrackedObj;
	private bool shortisRunning;
	private bool longisRunning;


	public void StartShortVibration()
	{
		if(shortisRunning)
			return;
		StartCoroutine (ShortVibration ());
	}

	public void StartLongVibration()
	{
		if(longisRunning)
			return;
		StartCoroutine (LongVibration ());
	}


	IEnumerator LongVibration()
	{
		for (float i = 0; i < 0.25f; i += Time.deltaTime)
		{
			longisRunning = true;
			SteamVR_Controller.Input((int)associatedTrackedObj.index).TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, 0.5f));
			yield return null;
			longisRunning = false;

		}
	}

	IEnumerator ShortVibration()
	{
		for (float i = 0; i < 0.05f; i += Time.deltaTime)
		{
			shortisRunning = true;
			SteamVR_Controller.Input((int)associatedTrackedObj.index).TriggerHapticPulse((ushort) 3999);
			yield return null;
			shortisRunning = false;
		}
	}
}
