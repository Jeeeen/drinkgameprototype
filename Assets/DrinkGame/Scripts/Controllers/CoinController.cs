﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

    private GameObject[] _coins = new GameObject[8];
    private float beatTime = 0.5f;
    //    private float beatTime = 1f;
//    public float coinJump = 0f;

//    public float coinScaleUp = 0.001f;
//    public float coinScaleUp = 0.00001f;

    private int curCoinReward;

    private int coinDiss;

    public bool CoinDissapears = false;



    public int GetCurCoinReward()
    {
        int  countActiveCoins = 0;

        for (int i = 0; i < _coins.Length; i++)
        {
            if (_coins[i].activeSelf)
                countActiveCoins++;
        }

        return countActiveCoins;
    }

	void Start () 
    {
        for (int i = 0; i < _coins.Length; i++)
        {
            _coins[i] = transform.GetChild(i).gameObject;
        }

        SetActiveAllCoins(false);

        WWW www = new WWW("MY URL");

	}

    public void StartCoinAnimation() 
    {
        Invoke("ShowAllCoins", GameManager.I._soundManager.beatTime);

        InvokeRepeating("PlayCoinSound", GameManager.I._soundManager.beatTime * 2  - GameManager.I._soundManager.coinSoundOffset, GameManager.I._soundManager.beatTime );

        InvokeRepeating("CoinDissapearing",  GameManager.I._soundManager.beatTime * 2, GameManager.I._soundManager.beatTime);

    }

    private void ShowAllCoins()
    {
        SetActiveAllCoins(true);
        CoinDissapears = false;
    }

    private void CoinDissapearing()
    {
        if (_coins[coinDiss].activeSelf)
            _coins[coinDiss].SetActive(false);

        coinDiss++;

        if (coinDiss >= _coins.Length)
        {
            coinDiss = 0;
            CancelInvoke();
        }
    }

    private void PlayCoinSound()
    {
        if(!CoinDissapears)
        GameManager.I._soundManager.PlayCoinDissapearSound();
    }

//    IEnumerator CoinAnimations()
//    {
//        yield return new WaitForSeconds(beatTime);
//
//        SetActiveAllCoins(true);    
//        // 1
//        yield return new WaitForSeconds(beatTime/2);
//
//
//          for (float i = 0; i < beatTime/2; i += Time.deltaTime)
//        {
//            for (int k = 0; k < _coins.Length; k++)
//            {
//                _coins[k].transform.localScale = new Vector3 ( _coins[k].transform.localScale.x,  _coins[k].transform.localScale.y +  coinScaleUp,  _coins[k].transform.localScale.z);
//                _coins[k].transform.localPosition = new Vector3 ( _coins[k].transform.localPosition.x,  _coins[k].transform.localPosition.y +  coinJump,  _coins[k].transform.localPosition.z);
//            }
//
//            yield return null;
//        }
//
//        DissapearCoin(_coins[0]);
//  
//        for (int t = 0; t < 6; t++)
//        {
//            
//            for (float i = 0; i < beatTime/2; i += Time.deltaTime)
//            {
//                for (int k = 0; k < _coins.Length; k++)
//                {
//                    _coins[k].transform.localScale = new Vector3 ( _coins[k].transform.localScale.x,  _coins[k].transform.localScale.y -  coinScaleUp,  _coins[k].transform.localScale.z);
//                    _coins[k].transform.localPosition = new Vector3 ( _coins[k].transform.localPosition.x,  _coins[k].transform.localPosition.y -  coinJump,  _coins[k].transform.localPosition.z);
//
//                }
//                yield return null;
//            }
//
//            for (float i = 0; i < beatTime/2; i += Time.deltaTime)
//            {
//                for (int k = 0; k < _coins.Length; k++)
//                {
//                    _coins[k].transform.localScale = new Vector3 ( _coins[k].transform.localScale.x,  _coins[k].transform.localScale.y +  coinScaleUp,  _coins[k].transform.localScale.z);
//                    _coins[k].transform.localPosition = new Vector3 ( _coins[k].transform.localPosition.x,  _coins[k].transform.localPosition.y +  coinJump,  _coins[k].transform.localPosition.z);
//                }
//                yield return null;
//            }
//
//            DissapearCoin(_coins[t + 1]);
//        }
//
//        // 8
//        for (float i = 0; i < beatTime/2; i += Time.deltaTime)
//        {
//            for (int k = 0; k < _coins.Length; k++)
//            {
//                _coins[k].transform.localScale = new Vector3 ( _coins[k].transform.localScale.x,  _coins[k].transform.localScale.y -  coinScaleUp,  _coins[k].transform.localScale.z);
//                _coins[k].transform.localPosition = new Vector3 ( _coins[k].transform.localPosition.x,  _coins[k].transform.localPosition.y - coinJump,  _coins[k].transform.localPosition.z);
//            }
//            yield return null;
//        }
//
//        yield return new WaitForSeconds(beatTime / 2);
//
//        DissapearCoin(_coins[7]);
//
//    }

    public void SetActiveAllCoins(bool active)
    {
        for (int i = 0; i < _coins.Length; i++)
        {
            _coins[i].SetActive(active);
        }
    }

    private void PlayCoinDissapearSound()
    {
        GameManager.I._soundManager.PlayCoinDissapearSound();
    }

    private void DissapearCoin(GameObject coin)
    {
        if (coin.activeSelf)
        {
            coin.SetActive(false);
            GameManager.I._soundManager.PlayCoinDissapearSound();
        }
    }

   
	

}
