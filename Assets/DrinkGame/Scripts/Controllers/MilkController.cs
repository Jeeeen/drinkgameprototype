﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;

public class MilkController : MonoBehaviour {

	public GameObject Cap;
	public bool IsCapPoped;
	public VRTK_InteractableObject interactableObj; 
	private float _popCapUpSpeed = 0.05f;
	private float _popCapDestroyTime = 5f;

	void Start ()
	{
		interactableObj = GetComponent<VRTK_InteractableObject> ();
	}
	
	void Update () 
	{
		UpdateMovePopCap ();

		TestMilkPopCap ();

		if(interactableObj!=null)
		{
			if(interactableObj.IsGrabbed())
			{
				if ( interactableObj.GetGrabbingObject ().GetComponent<VibrationController>().associatedTrackedObj.GetComponent<SteamVR_TrackedController>().padPressed && interactableObj.IsGrabbed() && !IsCapPoped) 
				{
					PopCap ();
					DestroyCapAfterPop (_popCapDestroyTime);
					interactableObj.GetGrabbingObject ().GetComponent<VibrationController>().StartShortVibration();
					GameManager.I.raiseOnMilkPopCapEvent (GameManager.I._soundManager._currentDoubleBeatNumber);
				}
			}
		}
	}
		
	public void PopCap()
	{
		IsCapPoped = true;
	}

	private void UpdateMovePopCap()
	{
		if (IsCapPoped && Cap != null ) 
		{
			Cap.transform.position = new Vector3 (Cap.transform.position.x, Cap.transform.position.y + _popCapUpSpeed,Cap.transform.position.z);
			RotateCap ();
		}	
	}

	private void RotateCap()
	{
		Cap.transform.Rotate (Vector3.right * 360f * Time.deltaTime); 
	}

	public void TestMilkPopCap()
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{ 	
			PopCap ();
			DestroyCapAfterPop (_popCapDestroyTime);
			GameManager.I.raiseOnMilkPopCapEvent (GameManager.I._soundManager._currentDoubleBeatNumber);
		}
	}

	public void TestMilkServedEvent()
	{
		if(Input.GetKeyDown(KeyCode.KeypadEnter))
			GameManager.I.raiseOnServedEvent (RequestType.Milk, GameManager.I._soundManager._currentIntBeatNumber, 0);
	}

	private void DestroyCapAfterPop(float destroyTime)
	{
		Destroy (Cap, destroyTime);
	}

	void OnCollisionEnter(Collision coll)
	{
		if (!coll.gameObject.CompareTag (Constants.CounterTag)) 
		{
			GameManager.I._soundManager.PlayObjectImpactSound ();
		}
	}
}
