﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixRequestPnlController : MonoBehaviour {

	private GameObject[] panelDrinks = new GameObject[6];

	public void SetActiveDrinks(List<MixedDrink> activeDrinks)
	{
		for (int i = 0; i < transform.childCount; i++) 
		{
			panelDrinks [i] = transform.GetChild (i).gameObject;
		}

		for (int i = 0; i < activeDrinks.Count; i++) 
		{
			panelDrinks [(int)activeDrinks [i]].SetActive (true);
		}
	}
}
