﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuddingController : MonoBehaviour 
{
	void OnCollisionEnter(Collision coll)
	{
		if (!coll.gameObject.CompareTag (Constants.CounterTag)) 
		{
			GameManager.I._soundManager.PlayObjectImpactSound ();
		}
	}
}
