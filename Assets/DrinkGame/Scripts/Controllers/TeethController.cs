﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TeethController : MonoBehaviour {
	[Header("Teeth Number Id")]
	public int TeethId;

	[HideInInspector]
	public int[] TeethRequests;

	private Renderer rend;
	private int touchBeat;
	private int currentStage = 1;
	private Rigidbody teethRigidBody;
	private int curRightBeat;

	private Request currentRequest;


	public void Init () 
	{
		TeethRequests = GameManager.I._songManager.GameSongs [GameManager.I._songManager.CurrentBgSong.SongID].GetComponentInChildren<TeethsRequest>().GetTeethRequest(TeethId);
		rend = GetComponent<Renderer> ();
		SetDefaultTextureOffset ();
		teethRigidBody = GetComponent<Rigidbody> ();
	}

	public void OnEnable()
	{
		GameManager.EveryBeatEvent += OnEveryBeat;
	}

	public void OnDisable()
	{
		GameManager.EveryBeatEvent -= OnEveryBeat;
	}

	public void OnEveryBeat(int beatNumber)
	{
		CheckRequests (beatNumber);
		CheckIsRequestLate();
	}

	private void CheckRequests(int beatNumber)
	{
		for (int i = 0; i < TeethRequests.Length; i++) 
		{
			if (TeethRequests [i] == beatNumber) 
			{
				Coloring ();
				currentRequest = new Request ();
				currentRequest.beatNumber = beatNumber;
				currentRequest.requestType = RequestType.Teeth;
				currentRequest.rightBeat = GameManager.I._soundManager._currentIntBeatNumber + 1;
			}	
		}
	}

	private void SetDefaultTextureOffset()
	{
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 1f));
		rend.material.SetTextureScale("_MainTex", new Vector2(0, 0.2f));

	}

	private void FirstTextureOffset()
	{
		rend.material.SetTextureScale("_MainTex", new Vector2(1, 1f));
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 0.9f));
	}

	private void SecondTextureOffset()
	{
		rend.material.SetTextureScale("_MainTex", new Vector2(1, 1f));
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 0.95f));
	}

	private void ThirdTextureOffset()
	{
		rend.material.SetTextureScale("_MainTex", new Vector2(1, 1f));
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 0));
	}

	private void FourthTextureOffset()
	{
		rend.material.SetTextureScale("_MainTex", new Vector2(1, 1f));
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 0.15f));
	}

	private void FifthTextureOffset()
	{
		rend.material.SetTextureScale("_MainTex", new Vector2(1, 1f));
		rend.material.SetTextureOffset("_MainTex", new Vector2(0, 0.25f));
	}


	private void Coloring()
	{
		InvokeRepeating ("FillingUpWithColor",0,0.1f);
	}

	private void FillingUpWithColor()
	{
		switch(currentStage)
		{
		case 1:
			FirstTextureOffset ();
			currentStage++;
			break;
		case 2:
			SecondTextureOffset ();
			currentStage++;
			break;
		case 3:
			ThirdTextureOffset ();
			currentStage++;
			break;
		case 4:
			FourthTextureOffset ();
			currentStage++;
			break;
		case 5:
			FifthTextureOffset ();
			currentStage = 1;
			CancelInvoke ();
			break;
		}
	}

	private void RemoveColor()
	{
		SetDefaultTextureOffset ();
	}

	public void FlyingAway()
	{
		GetComponent<MeshCollider> ().isTrigger = false;
		teethRigidBody.isKinematic = false;
		Destroy (gameObject,3f);
	}

//	private void TestTeeth()
//	{
//		if (Input.GetKeyDown (KeyCode.Space) && gameObject.name == "Teeth_1") 
//		{
//			RemoveColor ();
//			PlayTeethSound (TeethId);
//			HitTheBeat (GameManager.I._soundManager._currentIntBeatNumber);
//			Destroy(gameObject);
//		}
//	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Teeth") || other.gameObject.name == "pSphere1") 
		{
			return;
		}

		RemoveColor ();
		PlayTeethSound (TeethId);
		HitTheBeat (GameManager.I._soundManager._currentIntBeatNumber);
//		FlyingAway ();
		GameManager.I.raiseTeethHitEvent (TeethId);

		if (other.gameObject.transform.parent.parent.GetComponent<VibrationController> () != null)
		{
			other.gameObject.transform.parent.parent.GetComponent<VibrationController>().StartShortVibration();
		}
			
		Destroy(gameObject);

	
//		GetComponent<TeethController> ().enabled = false;
	}




	private void HitTheBeat(int curBeatNumber)
	{
		if (currentRequest == null || currentRequest.isChecked )
			return;
		
		if (curBeatNumber == currentRequest.rightBeat) 
		{
			Debug.Log ("Right");
			GameManager.I._scoreManager.UpdatePlayerScore (RequestResult.Right);
			currentRequest.isChecked = true;
		}

		if (curBeatNumber < currentRequest.rightBeat) 
		{
			Debug.Log ("Early");
			GameManager.I._scoreManager.UpdatePlayerScore (RequestResult.Early);
			currentRequest.isChecked = true;
		}
	}

	void CheckIsRequestLate()
	{
		if (currentRequest!= null && !currentRequest.isChecked)
		{
			if (currentRequest.rightBeat < GameManager.I._soundManager._currentIntBeatNumber) 
			{
				currentRequest.result = RequestResult.Late;
				currentRequest.isChecked = true;
				GameManager.I._scoreManager.UpdatePlayerScore (currentRequest.result);
			}
		}
	}

	private void PlayTeethSound(int teethId)
	{
		GameManager.I._soundManager.PlayTeethSound (teethId);
	}
//	
//	private void TestHitTheBeat()
//	{
//		if (Input.GetKeyDown (KeyCode.Space) && (gameObject.name == "Teeth_1")|| gameObject.name == "Teeth_1(Clone)")
//		{
//			HitTheBeat (GameManager.I._soundManager._currentIntBeatNumber);
//			//		FlyingAway ();
//			GameManager.I.raiseTeethHitEvent (0);
//			Destroy(gameObject);	
//		}
//	}

}
