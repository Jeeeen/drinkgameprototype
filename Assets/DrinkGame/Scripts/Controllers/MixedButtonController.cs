﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MixedButtonController : VRTK_InteractableObject
{
	private bool physic = false;
	private bool buttonPressed;
	private bool isCatched;

	private void Start()
	{
		SetButtonPhysic (false);
	}

	public override void StartTouching(GameObject currentTouchingObject)
	{
		base.StartTouching (currentTouchingObject);

		if (physic && !buttonPressed) 
		{
			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.ButtonPressPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.BtnPress));

			currentTouchingObject.GetComponent<VibrationController> ().StartShortVibration ();

			var grabController = currentTouchingObject.GetComponent<VRTK_ControllerActions>();

			grabController.TriggerHapticPulse (1,0.25f,0.01f);

			ButtonPress ();
		}
	}

	public override void Grabbed(GameObject currentTouchingObject)
	{

		if (gameObject.GetComponent<Rigidbody> () == null)
			return;
		
		GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.CatchingPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.BtnCatch));

		base.Grabbed (currentTouchingObject);
	}

	public void ButtonPress()
	{
		buttonPressed = true;
		StartCoroutine (StartBtnAnimWithDelay ());
	}

	public void StartButtonAnimation()
	{
		StartCoroutine (ButtonAnim ());
	}

	public IEnumerator StartBtnAnimWithDelay()
	{
		if (GameManager.I._soundManager.IsOnBeat ()) 
		{ 
			Debug.Log ("Start1");
			int curBeat = GameManager.I._soundManager._currentIntBeatNumber;
			while ( GameManager.I._soundManager._currentIntBeatNumber != curBeat+1)
			yield return new WaitForSeconds (0.001f);
			StartCoroutine (ButtonAnim ());
		}

		if (!GameManager.I._soundManager.IsOnBeat ()) 
		{ 
			Debug.Log ("Start2");

			while (!GameManager.I._soundManager.IsOnBeat ())
			yield return new WaitForSeconds (0.001f);
			StartCoroutine (ButtonAnim ());
		}
	}


	public IEnumerator ButtonAnim()
	{
		GameManager.I._soundManager.PlayBlendSound ();

		//FIRST BEAT   
		Debug.Log ( " mix button 1 up  on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat" );

		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y + 0.1f, transform.localScale.z);
			yield return null;
		}

		Debug.Log ( " mix button  1 down on "  + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");


		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y - 0.1f, transform.localScale.z);
			yield return null;
		}

		Debug.Log ("  mix button 2 up on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");

		//Second BEAT 
		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y + 0.1f, transform.localScale.z);
			yield return null;
		}	

		Debug.Log ("  mix button 2 down on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat" );


		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y - 0.1f, transform.localScale.z);
			yield return null;
		}

		Debug.Log ( " mix button 3 up  on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");

		//Third BEAT 

		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y + 0.1f, transform.localScale.z);
			yield return null;
		}

//		Debug.Log (" mix button 3 down " +GameManager.I._soundManager._currentDoubleBeatNumber + "beat" );

//
//		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
//		{
//			transform.localScale = new Vector3 (transform.localScale.x, transform.localScale.y - 0.1f, transform.localScale.z);
//			yield return null;
//		}

		Debug.Log (" mix button finish on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");

		SetButtonPhysic(true);
		LaunchButtonMath ();
	}


	public void  SetButtonPhysic(bool physic)
	{
		if (physic)
		{
			GetComponent<BoxCollider> ().isTrigger = false;
		}
		else
		{
			GetComponent<BoxCollider> ().isTrigger = true;
		}

		this.physic = physic;
	}

//	public void LaunchButtonPhysic()
//	{
//		var rb = gameObject.AddComponent<Rigidbody> ();
//		rb.AddForce (Vector3.up * buttonJumpForce);
//		SpawnAnotherButton ();
//		GameManager.I._soundManager.PlayFlySound ();
//	}

	private void SpawnAnotherButton()
	{
		transform.parent.gameObject.GetComponent<MixDrinkTriger> ().SpawnMixButton ();
	}

	public void LaunchButtonMath()
	{
		StartCoroutine (LaunchBtnMath());
	}

	public IEnumerator LaunchBtnMath()
	{
		GameManager.I._soundManager.PlayFlySound ();

		Debug.Log ("mix buttion start fly  on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");


		for (float i = 0; i < GameManager.I._soundManager.beatTime/2f; i += Time.deltaTime)
		{
			transform.position = new Vector3 (transform.position.x, transform.position.y + 0.035f, transform.position.z);
			yield return null;
		}

		Debug.Log ("mix button finish fly  on " + GameManager.I._soundManager._currentDoubleBeatNumber + "beat");

        if (gameObject.GetComponent<Rigidbody>() == null)
        {
            var rb = gameObject.AddComponent<Rigidbody>();
        }

		SpawnAnotherButton ();

//		for (float i = 0; i < GameManager.I._soundManager.beatTime / 2f; i += Time.deltaTime) {
//			transform.position = new Vector3 (transform.position.x, transform.position.y - 0.1f, transform.position.z);
//			yield return null;
		}

	}

public class MixedBeat
{
	public int beatNumber;
	public MixAction mixAction;

	public MixedBeat(int beatNumber, MixAction mixAction)
	{
		this.beatNumber = beatNumber;
		this.mixAction = mixAction;
	}

	public MixedBeat()
	{
		this.mixAction = MixAction.None;
	}
}




