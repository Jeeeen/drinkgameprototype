﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class CashBoxController : MonoBehaviour
{
	public Vector3 MoneySpawnPosition = new Vector3(0.455f,0,-0.05f);
	private  GameObject currentMoneyGo;

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.MoneyTag)) 
		{
			var grabbingObj = other.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ();
			SpawnMoneyInACashMachine(grabbingObj.GetComponent<VibrationController>().associatedTrackedObj.gameObject,other.GetComponent<MoneyController>().AssociatedCatID);
			grabbingObj.GetComponent<VRTK_InteractGrab> ().ForceRelease ();
			Destroy (other.gameObject);
		}
	}

	public void SpawnMoneyInACashMachine(GameObject conntrollerGO, int CatID)
	{
		GameObject moneyPrefab = Resources.Load("Prefabs/MoneyAttached") as GameObject;
		currentMoneyGo = Instantiate (moneyPrefab, this.transform);
		currentMoneyGo.transform.localPosition = MoneySpawnPosition;
		currentMoneyGo.transform.localEulerAngles = new Vector3 (0, -90, -90);
		currentMoneyGo.transform.SetParent(conntrollerGO.transform);
		currentMoneyGo.GetComponent<MoneyAttachedController>().associatedController = conntrollerGO;
		currentMoneyGo.GetComponent<MoneyAttachedController>().AssociatedCatID = CatID;
	}
}
