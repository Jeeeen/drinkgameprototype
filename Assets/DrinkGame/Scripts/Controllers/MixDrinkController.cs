﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MixDrinkController : VRTK_InteractableObject 
{ 
	public MixedDrink mixedDrinkType;
//	private float SpawnDelay = 0.5f;
	private float SpawnDelay = 0f;


	public override void Grabbed (GameObject currentGrabbingObject)
	{
		ActivatePhysic ();

		StartCoroutine (WaitSpawnDelay ());

		base.Grabbed (currentGrabbingObject);
	}

	IEnumerator WaitSpawnDelay()
	{
		yield return new WaitForSeconds (SpawnDelay);
		MixedDrinkSpawner.RaiseOnMixDrinkSpawnEvent (mixedDrinkType);
	}

	private void ActivatePhysic()
	{
		if (gameObject.GetComponent<Rigidbody> () == null) 
		{
			gameObject.AddComponent<Rigidbody> ();
		}

		if (gameObject.GetComponent<BoxCollider> ().isTrigger)
		{
			gameObject.GetComponent<BoxCollider> ().isTrigger = false;
		}
	}
}
