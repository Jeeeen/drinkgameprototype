﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyAttachedController : MonoBehaviour 
{
	private Vector3 swipeAttachedPos;
	private Quaternion startRot;
	public GameObject associatedController;
	public int AssociatedCatID;

	void Awake()
	{
		startRot = transform.rotation;
	}
	void Start()
	{
		swipeAttachedPos = transform.position;
	}

	void LateUpdate()
	{
		transform.position  =  new Vector3(swipeAttachedPos.x,transform.position.y,swipeAttachedPos.z);
		transform.rotation = startRot;
	}
}
