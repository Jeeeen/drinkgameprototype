﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatController : MonoBehaviour {
	
	[Header("Cat Number Id")]
	public int CatId;
	private List<Request> CatRequest;
	private Request currentRequest;
	[HideInInspector]
	public GameObject moneyObj;
	[HideInInspector]
	public List<int> PrerequestBeats;
	private Vector3 moneyBackPos = new Vector3(0,0.4f,0);
	private Vector3 moneyForwardPos = new Vector3(0,-0.5f,0);
	private Vector3 bubbleMixPos = new Vector3 (-0.235f, -0.214f, 0.224f);
	private float catJump = 0.2f;
	private Vector3 startPos;
	private GameObject bubblePrefab;
	private GameObject bubbleObject;
	public List<MixedDrink> currMixDrinks;
    public CoinController associatedCoinController;
    public GameObject milkSprite;
    public GameObject puddingSprite;


	void Update()
	{
		CheckIsLate ();
		ChekToDestroyMoney();
	}

	public void OnEnable()
	{
		GameManager.EveryBeatEvent += OnEveryBeat;
		GameManager.EveryHalfBeatEvent += OnEveryHalfBeat;
		GameManager.ServedEvent += OnServedEvent;
		GameManager.MilkPopCapEvent +=  OnMilkPopCapEvent;
		GameManager.MixedDrinkServeEvent += OnMixedDrinkServeEvent;
        GameManager.ServedRewardEvent += OnServedRewardEvent;
	}
	public void OnDisable()
	{
		GameManager.EveryBeatEvent -= OnEveryBeat;
		GameManager.ServedEvent -= OnServedEvent;
		GameManager.MilkPopCapEvent -= OnMilkPopCapEvent;
		GameManager.EveryHalfBeatEvent -= OnEveryHalfBeat;
		GameManager.MixedDrinkServeEvent -= OnMixedDrinkServeEvent;
        GameManager.ServedRewardEvent -= OnServedRewardEvent;
	}

    void OnServedRewardEvent (RequestType drinktype, int beatNumber, int catId, int rewardCoins, OnBeatHitResult onBeatHitResult)
    {
        if(currentRequest == null)
			return;
        if (catId != CatId)
			return;
        CheckRewardRequestResult(drinktype, beatNumber, catId, rewardCoins, onBeatHitResult);
    }

	#region event Handlers
	void OnMilkPopCapEvent (double beatNumber)
	{

        Debug.Log("[CatController] OnMilkPopCapEvent" + beatNumber);

		if ( currentRequest == null || currentRequest.requestType != RequestType.Milk)
			return;
		
		CheckMilkPopCap (beatNumber);
	}


	void OnServedEvent (RequestType drinktype, int beatNumber, int catId)
	{
		if(currentRequest == null)
			return;
		if (catId != CatId)
			return;
		CheckRequestResult (drinktype,beatNumber,currentRequest.rightBeat);
	}

	void OnEveryBeat(int beatNumber)
	{
		CheckRequests (beatNumber);
		CheckIsUpOnPrerequestedBeat();
		CheckForMoneyForPrequestedBeat();
	}

	void OnEveryHalfBeat(float beatNumber) { }

	void OnMixedDrinkServeEvent (MixedDrink drinkType, int beatNumber, MixTriggerSide side)
	{
		if(currentRequest == null || currentRequest.requestType != RequestType.Mix )
			return;

		CheckMixRequestResult (drinkType,beatNumber,CatId,side);
	}


	#endregion


	public void Init()
	{
		startPos = transform.position;

        if(GameManager.I.GAME_MODE == GameMode.Simple || GameManager.I.GAME_MODE == GameMode.Shark || GameManager.I.GAME_MODE == GameMode.Reward)
		CatRequest = GameManager.I._songManager.GameSongs [GameManager.I._songManager.CurrentBgSong.SongID].transform.GetChild (CatId).GetComponent<CatRequests>().GetAllCatRequests();

		else if(GameManager.I.GAME_MODE == GameMode.Mix )
		CatRequest = GameManager.I._songManager.GameSongs [GameManager.I._songManager.CurrentBgSong.SongID].transform.GetChild (CatId).GetComponent<MixRequests>().GetAllCatMixRequests();

			for (int i = 0; i < CatRequest.Count; i++) 
		{
			PrerequestBeats.Add(CatRequest[i].beatNumber - 1);
		}

		bubblePrefab = Resources.Load (Constants.BubbleMixPrefabPath) as GameObject;
       
	}

	public void RestartCat()
	{
        
		transform.position = startPos;

        if(GameManager.I.GAME_MODE == GameMode.Simple || GameManager.I.GAME_MODE == GameMode.Shark || GameManager.I.GAME_MODE == GameMode.Reward)
			CatRequest = GameManager.I._songManager.GameSongs [GameManager.I._songManager.CurrentBgSong.SongID].transform.GetChild (CatId).GetComponent<CatRequests>().GetAllCatRequests();

		else if(GameManager.I.GAME_MODE == GameMode.Mix )
			CatRequest = GameManager.I._songManager.GameSongs [GameManager.I._songManager.CurrentBgSong.SongID].transform.GetChild (CatId).GetComponent<MixRequests>().GetAllCatMixRequests();
	}

	void CheckRequests(int beatNumber)
	{

		for (int i = 0; i < CatRequest.Count; i++) 
		{
			if (CatRequest [i].beatNumber == beatNumber) 
			{
				SetCurrentRequest (CatRequest [i]);
				MoveUp ();
				GameManager.I.StartRequest (CatRequest [i],CatId);

                if (GameManager.I.GAME_MODE == GameMode.Reward)
                {
					UpdateRequestImg(CatRequest [i].requestType,true);

                    associatedCoinController.StartCoinAnimation();
                }
                    
				if (CatRequest [i].requestType == RequestType.Mix) 
				{
					SpawnRequestBubble (CatRequest [i].mixedDrinks);
				}
			}	
		}
	}

	public void CheckRequestResult(RequestType drinktype,int realBeatNumber, int rigtBeatNumber)
	{
		if (currentRequest.isChecked) 
			return;

		var requestResult = RequestResult.Error;

		if (drinktype != currentRequest.requestType) 
		{
			requestResult = RequestResult.WrongDrink;
		}

		else if (realBeatNumber < currentRequest.rightBeat)
		{
			requestResult = RequestResult.Early;
		}

		else if (realBeatNumber >currentRequest.rightBeat)
		{
			requestResult = RequestResult.Late;
		}

		else if (realBeatNumber == currentRequest.rightBeat)
		{
			if(drinktype == RequestType.Milk && currentRequest.isMilkPopCapGood)
				
				requestResult = RequestResult.Right;

			if (drinktype == RequestType.Milk && !currentRequest.isMilkPopCapGood)
				
				requestResult = RequestResult.MilkFailPopCap;
			else 
			{
				requestResult = RequestResult.Right;
			}
		}

		if(requestResult == RequestResult.Early)
		{
			GameManager.I.raiseOnEarlyServedEvent();
		}
		currentRequest.result = requestResult;
        if(currentRequest.requestType !=RequestType.Mix )
		currentRequest.isChecked = true;
		MoveDown ();
		GameManager.I._scoreManager.UpdatePlayerScore (currentRequest.result);
	}


    public void CheckRewardRequestResult(RequestType drinktype, int beatNumber, int catId, int coinReward, OnBeatHitResult onBeatHitResult)
    {
   
        if (currentRequest.isChecked || currentRequest == null || catId != this.CatId) 
            return;


        var requestResult = RequestResult.Error;

        if (onBeatHitResult == OnBeatHitResult.OffBeat)
        {
            requestResult = RequestResult.OffBeat;
        }

        if ( drinktype != currentRequest.requestType)
        {
            requestResult = RequestResult.WrongDrink;
        }

           if (onBeatHitResult == OnBeatHitResult.OnBeat && drinktype == currentRequest.requestType)
        {
            requestResult = RequestResult.Right;
        }

        currentRequest.result = requestResult;
        if(currentRequest.requestType !=RequestType.Mix)
            currentRequest.isChecked = true;

		if(GameManager.I.GAME_MODE == GameMode.Reward)
        UpdateRequestImg(drinktype, false);
        
        MoveDown ();

        GameManager.I._scoreManager.UpdatePlayerCoins (currentRequest.result, coinReward,onBeatHitResult);

    }


	public void CheckMixRequestResult(MixedDrink drinktype,int realBeatNumber, int rigtBeatNumber,MixTriggerSide side)
	{
		if(currentRequest.isChecked)
			return;

		bool rightDrink = false;
		bool consistAll = true;;

		for (int i = 0; i < currentRequest.mixedDrinks.Count; i++)
		{
			if(currentRequest.mixedDrinks[i] == drinktype)
			rightDrink  = true;
		}

		if (rightDrink)
			currMixDrinks.Add (drinktype);
		else
		{
			GameManager.I.raiseMixFinishEvent (false,side);
			currentRequest.isChecked = true;
			DestroyRequestBubble ();
			MoveDown();
		}

		for (int i = 0; i < currentRequest.mixedDrinks.Count; i++) 
		{
			if (!currMixDrinks.Contains (currentRequest.mixedDrinks [i])) 
			{
				consistAll = false;
				Debug.Log ("Still not all bottles");
			}
		}

		if (consistAll)
		{
			GameManager.I.raiseMixFinishEvent (true,side);
			currentRequest.isChecked = true;
			Debug.Log ("Serve All Bottles");
			DestroyRequestBubble ();
			MoveDown();
		}
	}

	void CheckIsLate()
	{
		if (currentRequest!= null && currentRequest.requestType == RequestType.Mix)
			return;
		
		if (currentRequest!= null && !currentRequest.isChecked)
			{
				if (currentRequest.rightBeat < GameManager.I._soundManager._currentIntBeatNumber) 
				{
				
					currentRequest.result = RequestResult.Late;

					currentRequest.isChecked = true;
					

				if(GameManager.I.GAME_MODE == GameMode.Reward)
                    
                    UpdateRequestImg(currentRequest.requestType, false);

					GameManager.I._scoreManager.UpdatePlayerScore (currentRequest.result);

					if (currentRequest.requestType == RequestType.Money) 
					{
						StartCoroutine(MoveMoneyAndCatBackOnTwoNextBeat());
					}
				}
			}
	}

	void SetCurrentRequest(Request curReq)
	{
		currentRequest = curReq;
	}

	void CheckMilkPopCap(double realBatNumber)
	{
		Debug.Log ("[Cat Controller] Real  Milk Pop Cap Beat Number= " + realBatNumber);

		if (realBatNumber == currentRequest.beatNumber + 3.5f ) 
		{
			currentRequest.isMilkPopCapGood = true;
		}
		else 
		{
			currentRequest.result = RequestResult.MilkFailPopCap;
			currentRequest.isChecked = true;
			MoveDown ();
			GameManager.I._scoreManager.UpdatePlayerScore (currentRequest.result);
		}
	}

    void CheckMilkPopCapRewardMode(double realBeatNumber)
    {
        Debug.Log ("[Cat Controller] Real  Milk Pop Cap Beat Number= " + realBeatNumber);

        if (GameManager.I._soundManager.GetOnBeatHitResult() == OnBeatHitResult.OnBeat)
        {
            GameManager.I.raiseBeatSuccessEvent(true,new MixedBeat());
        }

        else 
        {
            GameManager.I.raiseBeatSuccessEvent(false,new MixedBeat());
        }
    }

	public void CheckIsUpOnPrerequestedBeat()
	{
		for (int i = 0; i < PrerequestBeats.Count; i++) 
		{
			if (PrerequestBeats[i] == GameManager.I._soundManager._currentIntBeatNumber && !IsCatInDownPose() ) 
			{
				MoveDown();
			}	
		}
	}

	public void CheckForMoneyForPrequestedBeat()
	{
		for (int i = 0; i < PrerequestBeats.Count; i++) 
		{
			if (PrerequestBeats[i] == GameManager.I._soundManager._currentIntBeatNumber && CatRequest[i].requestType == RequestType.Money) 
			{
				if(IsMoneyUnAvailable())
				{
					MoneySpawn();
				}
				StartCoroutine(MoveMoneyOnNextBeat());
			}	
		}
	}

	#region Cat Position Methods

	public bool IsCatInDownPose()
	{
		return 	transform.position == startPos;
	}

	public bool IsCatInUpPose()
	{
		return 	transform.position.y == startPos.y+catJump;
	}

	public void MoveUp() // Moving Cat Transform Position to Up position
	{
		if(transform.position == startPos)
			transform.position = new Vector3(transform.position.x,transform.position.y + catJump,transform.position.z);
	}

	public void MoveDown()  // Moving Cat Transform Position to Up position
	{
		if(transform.position!= startPos)
			transform.position = new Vector3(transform.position.x,transform.position.y - catJump,transform.position.z);
	}

	#endregion Cat Position Handlers


	#region Money Methods

	public void MoneySpawn()
	{
		GameObject moneyPrefab = Resources.Load("Prefabs/Money") as GameObject;
		moneyObj = Instantiate (moneyPrefab, this.transform);
		moneyObj.transform.localPosition = moneyBackPos;
		moneyObj.transform.localEulerAngles = new Vector3 (0, -60, 0);
		moneyObj.GetComponent<MoneyController>().AssociatedCatID = CatId;
	}

	public bool IsMoneyUnAvailable()
	{
		return (moneyObj.gameObject == null || (!IsCatInDownPose()  && !IsCatInUpPose()));
	}

	public bool IsMoneyOutOfPlace()
	{
		return (moneyObj.transform.position.y < -5);
	}

	public void MoveMoneyForward()
	{
		if(moneyObj.gameObject != null)
			moneyObj.transform.localPosition = moneyForwardPos;
	}

	public void MoveMoneyBack()
	{
		if(moneyObj.gameObject != null)
			moneyObj.transform.localPosition = moneyBackPos;
	}

	public IEnumerator MoveMoneyOnNextBeat()
	{
		yield return new WaitForSeconds(GameManager.I._soundManager.beatTime);
		MoveMoneyForward();
	}

	public IEnumerator MoveMoneyAndCatBackOnTwoNextBeat()
	{
		yield return new WaitForSeconds(GameManager.I._soundManager.beatTime  + GameManager.I._soundManager.beatTime);
		MoveMoneyBack();
		MoveDown();
	}

	void ChekToDestroyMoney()
	{ 
		if(moneyObj.gameObject == null)
			return;
		if(IsMoneyOutOfPlace())
		{
			Destroy(moneyObj.gameObject);
		}
	}

	#endregion 

	#region Bubble Methods

	private void SpawnRequestBubble(List<MixedDrink> mixedDrinks)
	{
		bubbleObject= Instantiate (bubblePrefab, this.transform) as GameObject;
		bubbleObject.transform.localPosition = bubbleMixPos;
		bubbleObject.transform.localEulerAngles = new Vector3 (0, 180, 180);
		bubbleObject.GetComponent<MixRequestPnlController>().SetActiveDrinks (mixedDrinks);
//		GameManager.I._soundManager.PlaySpeechBubbleSound ();
	}

	private void DestroyRequestBubble()
	{
		Destroy (bubbleObject);
	}

	#endregion


    private void UpdateRequestImg(RequestType requestType,bool active)
    {
        switch (requestType)
        {
            case RequestType.Milk:
                {
                    milkSprite.SetActive(active);
                    break;
                }
            case RequestType.Pudding:
                {
                    puddingSprite.SetActive(active);
                    break;
                }
            default:
                break;
        }
    }
}
