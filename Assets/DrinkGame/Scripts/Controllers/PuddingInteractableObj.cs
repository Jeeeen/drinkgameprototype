﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;


public class PuddingInteractableObj : VRTK_InteractableObject {

	public override void Grabbed (GameObject currentGrabbingObject)
	{

		if (gameObject.GetComponent<Rigidbody> () == null) 
		{
			gameObject.AddComponent<Rigidbody> ();
		}

		if (gameObject.GetComponent<BoxCollider> ().isTrigger)
		{
			gameObject.GetComponent<BoxCollider> ().isTrigger = false;
		}

		base.Grabbed (currentGrabbingObject);
	}
}
