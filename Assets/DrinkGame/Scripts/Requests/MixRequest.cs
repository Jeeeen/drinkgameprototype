﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixRequest : MonoBehaviour
{
	public int beatNumber;
	private RequestType requestType = RequestType.Mix;
	public List<MixedDrink> requiredDrinks;
	public int CatId;

	public Request GetMixRequest()
	{
		var request = new Request (requestType, beatNumber, CatId,requiredDrinks);
		return request;
	}


}
