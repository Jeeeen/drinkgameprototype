﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatRequests : MonoBehaviour
{
	[Header("Pudding requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] PuddingBeats ;
	[Header("Milk requests happens on beas: ")]
	[Tooltip("Input beats number here")]
	public int[] MilkBeats;
	[Header("Money requests happens on beas: ")]
	[Tooltip("Input beats number here")]
	public int[] MoneyBeats;
	[Header("Cat Number Id")]
	public int CatId;

	public List<Request> catRequests = new List<Request>();

	public List<Request> GetAllCatRequests()
	{
		for (int i = 0; i < PuddingBeats.Length; i++)
		{
			catRequests.Add(new Request(RequestType.Pudding,PuddingBeats[i],CatId));
		}

		for (int i = 0; i < MilkBeats.Length; i++)
		{
			catRequests.Add(new Request(RequestType.Milk,MilkBeats[i],CatId));
		}


        if (GameManager.I.GAME_MODE == GameMode.Simple)
        {
            for (int i = 0; i < MoneyBeats.Length; i++)
            {
                catRequests.Add(new Request(RequestType.Money,MoneyBeats[i],CatId));
            }
        }

		return catRequests;
	}
}

