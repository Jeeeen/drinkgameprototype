﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeethsRequest : MonoBehaviour {

	[Header("First Teeth requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] FirstTeethRequests;

	[Header("Second Teeth requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] SecondTeethRequests;

	[Header("Third Teeth requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] ThirdTeethRequests;

	[Header("Fourth Teeth requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] FourthTeethRequests;

	[Header("Fifth Teeth requests happens on beats: ")]
	[Tooltip("Input beats number here")]
	public int[] FifthTeethRequests;

	[HideInInspector]
	public int[][] AllTeethsRequest = new int[5][];


	public int[] GetTeethRequest( int id)
	{
		AllTeethsRequest [0] = FirstTeethRequests;
		AllTeethsRequest [1] = SecondTeethRequests;
		AllTeethsRequest [2] = ThirdTeethRequests;
		AllTeethsRequest [3] = FourthTeethRequests;
		AllTeethsRequest [4] = FifthTeethRequests;

		return AllTeethsRequest [id];
	}
}
