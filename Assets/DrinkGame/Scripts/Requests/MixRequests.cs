﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixRequests : MonoBehaviour {

	public int catId;
	[HideInInspector]
	public List <Request> CatMixRequest = new List<Request>();

	public List <Request> GetAllCatMixRequests()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			CatMixRequest.Add(transform.GetChild (i).GetComponent<MixRequest> ().GetMixRequest ());
		}
		return CatMixRequest;
	}

	public void TestgEtChild()
	{
		transform.GetChild (0).GetComponent<MixRequest> ().GetMixRequest ();
	}
}
