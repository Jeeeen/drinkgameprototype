﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Request 
{
	public RequestType requestType;
	public int beatNumber;
	public int CatId;
	public int rightBeat;
	public bool isChecked;
	public RequestResult result;
	public bool isMilkPopCapGood;
	public List<MixedDrink> mixedDrinks;


	public Request (RequestType requestType, int beatNumber,int CatId)
	{
		this.requestType = requestType;
		this.beatNumber = beatNumber;
		this.CatId = CatId;
		this.rightBeat = GetRightBeatNumber (requestType, beatNumber);
        if(GameManager.I.GAME_MODE == GameMode.Reward)
            this.rightBeat = beatNumber + 9;
	}

	public Request (RequestType requestType, int beatNumber,int CatId, List<MixedDrink> mixedDrinks)
	{
		this.requestType = requestType;
		this.beatNumber = beatNumber;
		this.CatId = CatId;
		this.rightBeat = GetRightBeatNumber (requestType, beatNumber);
		this.mixedDrinks = mixedDrinks;
	}

	public Request ()
	{
		
	}

	public int GetRightBeatNumber(RequestType requestType ,int requestBeatNumber)
	{
		switch (requestType) 
		{
		case RequestType.Pudding:
			{
				return requestBeatNumber + 2;
			}
		case RequestType.Milk:
			{
				return requestBeatNumber + 4;
			}
		case RequestType.Money:
			{
				return requestBeatNumber + 4;
			}
		case RequestType.Mix:
			{
				return requestBeatNumber + 30;
			}
		default:
			return 0;
		}
	}
}

