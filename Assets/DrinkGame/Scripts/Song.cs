﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Song : MonoBehaviour 
{
	[Header("BPM Of The Background song")]
	[Tooltip("Input BPM of the song here")]
	public float BPM;

	[Header(" AudioClip of  the background song")]
	[Tooltip("Drag here audioclip of the song ")]
	public AudioClip AudioClip;

	[Header("Type song number here")]
	[Tooltip("Type song number here")]
	public int SongID;

	private List<Request> SongsRequests = new List<Request>();

	public List <Request> GetAllSongRequest()
	{
		for (int i = 0; i < transform.childCount; i++) 
		{
			if(transform.GetChild(i).GetComponent<CatRequests>()!=null)
			SongsRequests.AddRange(transform.GetChild(i).GetComponent<CatRequests>().GetAllCatRequests());
		}

		return SongsRequests;
	}
}
