﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UI_Controller : MonoBehaviour
{

	public Text CurrentBeat;
	public Text BeatSuccesText;
	public Text ScoreText;
	public Text ComboMultiplierText;
    public Text CoinRewardsText;
}
