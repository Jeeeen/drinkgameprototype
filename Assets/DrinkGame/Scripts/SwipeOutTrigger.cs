﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeOutTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.MoneyAttachedTag)) 
		{
			GameManager.I.raiseOnServedEvent (RequestType.Money, GameManager.I._soundManager._currentIntBeatNumber,other.GetComponent<MoneyAttachedController>().AssociatedCatID);
			other.gameObject.GetComponent<MoneyAttachedController>().associatedController.GetComponent<SteamVR_TrackedController>().associatedVRTKContrroller.GetComponent<VibrationController>().StartShortVibration();
			Destroy (other.gameObject);
		}
	}
}
