﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;

public class ServeTrigger : MonoBehaviour
{
	private Vector3 ServePosition = new Vector3(0,0.2f,0);
	public CatController associatedCatController;
    private CoinController associatedCoinController;


    void Start()
    {
        associatedCoinController = GetComponent<CoinController>();
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.MilkServeTag)) 
		{
			GameObject milk = other.transform.parent.gameObject;

			if (milk.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject () != null)
			{
				GameObject grabbingObj = milk.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ();

				var grabController = milk.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ().GetComponent<VRTK_ControllerActions>();

				grabController.TriggerHapticPulse (1,0.25f,0.01f);

//				grabbingObj.GetComponent<VibrationController>().StartLongVibration();
				grabbingObj.GetComponent<VRTK_InteractGrab>().ForceRelease ();
			}

			SpawnServedDrink(RequestType.Milk,milk.gameObject.GetComponent<MilkController>().IsCapPoped);

			Destroy (milk.gameObject);

            if (GameManager.I.GAME_MODE != GameMode.Reward)
                GameManager.I.raiseOnServedEvent(RequestType.Milk, GameManager.I._soundManager._currentIntBeatNumber, associatedCatController.CatId);
            else
            {

                GameManager.I.raiseServeRewardEvent (RequestType.Milk, GameManager.I._soundManager._currentIntBeatNumber, associatedCatController.CatId,associatedCoinController.GetCurCoinReward(),GameManager.I._soundManager.GetOnBeatHitResult());
                associatedCoinController.SetActiveAllCoins(false);
                associatedCoinController.CoinDissapears = true;
            }

		} 

		else if(other.CompareTag (Constants.PuddingServeTag))
		{
			GameObject pudding = other.transform.parent.gameObject;

			if (pudding.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject () != null)
			{
				var grabbingObj = pudding.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ();
				var grabController = pudding.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ().GetComponent<VRTK_ControllerActions>();

				grabController.TriggerHapticPulse (1,0.25f,0.01f);
				//			grabbingObj.GetComponent<VibrationController>().StartVibration();
				grabbingObj.GetComponent<VRTK_InteractGrab>().ForceRelease ();
			}

			SpawnServedDrink(RequestType.Pudding);

			Destroy(pudding.gameObject);


            if (GameManager.I.GAME_MODE != GameMode.Reward)
			    GameManager.I.raiseOnServedEvent (RequestType.Pudding, GameManager.I._soundManager._currentIntBeatNumber, associatedCatController.CatId);
            else
            {
				Debug.Log("raising Serving event");
                GameManager.I.raiseServeRewardEvent (RequestType.Pudding, GameManager.I._soundManager._currentIntBeatNumber, associatedCatController.CatId,associatedCoinController.GetCurCoinReward(),GameManager.I._soundManager.GetOnBeatHitResult());
                associatedCoinController.SetActiveAllCoins(false);
                associatedCoinController.CoinDissapears = true;
            }
		}
		else if(other.CompareTag (Constants.DrinkButtonTag))
		{
			GameObject mixedDrink = other.gameObject;

			if (mixedDrink.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject () != null)
			{
				var grabbingObj = mixedDrink.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ();
				var grabController = mixedDrink.gameObject.GetComponent<VRTK_InteractableObject> ().GetGrabbingObject ().GetComponent<VRTK_ControllerActions>();

				grabController.TriggerHapticPulse (1,0.25f,0.01f);
				//			grabbingObj.GetComponent<VibrationController>().StartVibration();
				grabbingObj.GetComponent<VRTK_InteractGrab>().ForceRelease ();
			}

			SpawnServedDrink(RequestType.Mix);
			GameManager.I.raiseCheckOnBeatMixEvent (GameManager.I._pointValues.ServingPoints,new MixedBeat(GameManager.I._soundManager._currentIntBeatNumber,MixAction.BtnServe));
			GameManager.I.raiseServeMixEvent(associatedCatController.CatId);
			Destroy (mixedDrink.gameObject);
//			GameManager.I.raiseOnServedEvent (RequestType.Mix, GameManager.I._soundManager._currentIntBeatNumber, associatedCatController.CatId);
		}

	}

	void SpawnServedDrink(RequestType drinkType,bool milkCapPoped = true)
	{
		switch (drinkType) 
		{
		case RequestType.Milk:
			{
				var drinkPrefab = (milkCapPoped)?Resources.Load(Constants.MilkServedPrefabPath) as GameObject:Resources.Load(Constants.MilkServedWithCapPrefabPath) as GameObject ;
				GameObject drink = Instantiate (drinkPrefab, this.transform);
				drink.transform.localPosition = ServePosition;
				Destroy (drink, GameManager.I._soundManager.beatTime);
//				GameManager.I.SpawnNewMilk();
				break;
			}
		case RequestType.Pudding:
			{
				GameObject drinkPrefab = Resources.Load(Constants.PuddingServedPrefabPath) as GameObject;
				GameObject drink = Instantiate (drinkPrefab, this.transform);
				drink.transform.localPosition = ServePosition;
				Destroy (drink, GameManager.I._soundManager.beatTime);
//				GameManager.I.SpawnNewPudding ();
				break;
			}
		case RequestType.Mix:
			{
				GameObject drinkPrefab = Resources.Load(Constants.ButtonDisplayPrefabPath) as GameObject;
				GameObject drink = Instantiate (drinkPrefab, this.transform);
				drink.transform.localPosition = new Vector3(-0.55f,0,0.02f);
				drink.transform.localEulerAngles = new Vector3(0,0,0);

				Destroy (drink, GameManager.I._soundManager.beatTime);
				break;
			}
		default:
			break;
		}
	}
}
