﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRequest : MonoBehaviour 
{
	[Header("Start Beat")]
	public int StartBeat;
	[Header("Cat Number Id")]
	public int CatId;
}

