﻿public enum RequestType
{
	Pudding,
	Milk,
	Money,
	Mix,
	MilkPopCap,
	Teeth
}

public enum RequestResult
{
	Error,
	Early,
	Late,
	WrongDrink,
	Right,
	MilkFailPopCap,
    OffBeat
}

public enum ControllerType
{
	Left,
	Right
}

public enum DrinkTypeSpawn
{
	Milk,
	Pudding,
	MilkFlip
}

public enum BeatSystem
{
	Old,
	New
}

public enum Sounds
{
	Collision
}

public enum MixedDrink
{
	orange,
	green,
	yellow,
	pink,
	cyan,
	blue
}

public enum GameMode
{
	Simple,
	Shark,
	Mix,
    Reward
}

public enum MixTriggerSide
{
	Left,
	Right
}

public enum MixAction
{
	None,
	Mix,
	BtnPress,
	BtnCatch,
	BtnServe
}

public enum OnBeatHitResult
{
    OnBeat,
    OffBeat
}
