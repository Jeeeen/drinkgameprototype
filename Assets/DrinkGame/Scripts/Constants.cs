﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
	#region Tags

	public const string MilkTag = "Milk";
	public const string PuddingTag = "Pudding";
	public const string MoneyTag = "Money";
	public const string MoneyAttachedTag = "MoneyAttached";
	public const string CounterTag = "Counter";
	public const string MilkServeTag = "milk_serve";
	public const string PuddingServeTag = "pudding_serve";
	public const string MixedDrinkTag = "mixed_drink";
	public const string DrinkButtonTag = "drink_button";



	#endregion  

	#region PrefabPaths

	public const string MilkServedPrefabPath = "Prefabs/Drinks/ServedMilk";
    public const string MilkServedWithCapPrefabPath = "Prefabs/Drinks/Milk_display";
    public const string PuddingServedPrefabPath = "Prefabs/Drinks/Pudding_display";
	public const string MilkPrefabPath = "Prefabs/Drinks/Milk";
    public const string MilkFlipPrefabPath = "Prefabs/Drinks/milk_flip";
	public const string PuddingPrefabPath = "Prefabs/Drinks/Pudding";

	//mixed drinks
	public const string OrangeMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/orange";
	public const string GreenMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/green";
	public const string YellowMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/yellow";
	public const string PinkMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/pink";
	public const string CyanMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/cyan";
	public const string BlueMixDrinkPrefabPath = "Prefabs/Drinks/MixedDrinks/blue";
	public const string BubbleMixPrefabPath = "Prefabs/Panels/Mix_request_pnl";
	public const string ButtonDisplayPrefabPath = "Prefabs/Drinks/button_display";


	public const string LeftDrinkButtonPrefabPath = "Prefabs/Drinks/left_btn";
	public const string RightDrinkButtonPrefabPath = "Prefabs/Drinks/right_btn";


	#endregion 
}
