﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MixedDrinkSpawner : MonoBehaviour 
{
	public static event Action<MixedDrink> SpawnMixedDrink;
	private Vector3[] SpawnPos = new Vector3[6];
	private Vector3[] SpawnScales = new Vector3[6];
	private GameObject[] mixDrinkPrefabs = new GameObject[6];
	private GameObject puddingPrefab;
	private GameObject milkPrefab;
	private GameObject drinkPrefab;
	private bool isFull;
	private bool isStay;

	void OnEnable()
	{
		SpawnMixedDrink += OnSpawnMixedDrink;
	}

	void OnDisable()
	{
		SpawnMixedDrink -= OnSpawnMixedDrink;
	}

	void OnSpawnMixedDrink (MixedDrink type)
	{
		SpawnDrink (type);
	}

	void Start () 
	{

		for (int i = 0; i < transform.childCount; i++)
		{
			if(transform.GetChild(i).gameObject.GetComponent<MixDrinkController>() != null)
				SpawnPos [i] = transform.GetChild (i).transform.position;
		}

		for (int i = 0; i < transform.childCount; i++)
		{
			if(transform.GetChild(i).gameObject.GetComponent<MixDrinkController>() != null)
				SpawnScales [i] = transform.GetChild (i).transform.localScale;
		}

		mixDrinkPrefabs [0] = Resources.Load (Constants.OrangeMixDrinkPrefabPath) as GameObject;
		mixDrinkPrefabs [1] = Resources.Load (Constants.GreenMixDrinkPrefabPath) as GameObject;
		mixDrinkPrefabs [2] = Resources.Load (Constants.YellowMixDrinkPrefabPath) as GameObject;
		mixDrinkPrefabs [3] = Resources.Load (Constants.PinkMixDrinkPrefabPath) as GameObject;
		mixDrinkPrefabs [4] = Resources.Load (Constants.CyanMixDrinkPrefabPath) as GameObject;
		mixDrinkPrefabs [5] = Resources.Load (Constants.BlueMixDrinkPrefabPath) as GameObject;

	}

	public void SpawnDrink(MixedDrink mixedDrink)
	{
	GameObject drink = Instantiate (mixDrinkPrefabs[(int) mixedDrink], this.transform);
	drink.transform.position = SpawnPos[(int) mixedDrink];
	drink.transform.localScale = SpawnScales[(int) mixedDrink];
	}

	public static void RaiseOnMixDrinkSpawnEvent(MixedDrink mixedDrinkType)
	{
		if(MixedDrinkSpawner.SpawnMixedDrink !=null)
			MixedDrinkSpawner.SpawnMixedDrink (mixedDrinkType);
	}
}
