﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeethSpawner : MonoBehaviour {

	private Vector3[] teethsPos = new Vector3[5];
	private GameObject[] TeethsPrefabs = new GameObject[5];
	private Quaternion[] teethsRot = new Quaternion[5];

	void Start()
	{
		for (int i = 0; i < teethsPos.Length; i++) 
		{
			teethsPos [i] = transform.GetChild (i).transform.position;
		}

		for (int i = 0; i < teethsRot.Length; i++) 
		{
			teethsRot [i] = transform.GetChild (i).transform.rotation;
		}

		TeethsPrefabs [0] = Resources.Load ("Prefabs/Teeths/Teeth_1") as GameObject;
		TeethsPrefabs [1] = Resources.Load ("Prefabs/Teeths/Teeth_2") as GameObject;
		TeethsPrefabs [2] = Resources.Load ("Prefabs/Teeths/Teeth_3") as GameObject;
		TeethsPrefabs [3] = Resources.Load ("Prefabs/Teeths/Teeth_4") as GameObject;
		TeethsPrefabs [4] = Resources.Load ("Prefabs/Teeths/Teeth_5") as GameObject;
	}

	void OnEnable()
	{
		GameManager.TeethHitEvent+= OnTeethHitEvent;
	}

	void OnDisable()
	{
		GameManager.TeethHitEvent -= OnTeethHitEvent;
	}

	void OnTeethHitEvent (int teethId)
	{
		StartCoroutine(SpawnTeeth (teethId));
	}

	IEnumerator SpawnTeeth(int teethID)
	{
		yield return new WaitForSeconds (1f);
		var teeth = Instantiate (TeethsPrefabs [teethID],this.transform) as GameObject;
		teeth.transform.position = teethsPos[teethID];
		teeth.transform.rotation = teethsRot [teethID];
		teeth.transform.localScale = new Vector3 (1, 1, 1);
		teeth.gameObject.GetComponent<TeethController> ().Init ();
	}


}
