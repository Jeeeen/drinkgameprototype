﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinitySpawner : MonoBehaviour 
{
	public DrinkTypeSpawn SpawnerType;
	private Vector3 SpawnPos = new Vector3(0,0.21f,0);
	private GameObject puddingPrefab;
	private GameObject milkPrefab;
	private GameObject drinkPrefab;
	private bool isFull;
	private bool isStay;

	void Start () 
	{

		CheckDrinkTypePrefab();
		if(SpawnerType != DrinkTypeSpawn.MilkFlip)
		SpawnDrink(drinkPrefab);

//		milkPrefab = Resources.Load (Constants.MilkPrefabPath) as GameObject;
//		puddingPrefab = Resources.Load (Constants.PuddingPrefabPath) as GameObject;
//

//		SpawnDrink ((SpawnerType == DrinkType.Milk) ? milkPrefab : puddingPrefab);

	}

	public void SpawnDrink(GameObject prefab)
	{
		GameObject drink = Instantiate (prefab, this.transform);
		drink.transform.localPosition = SpawnPos;
		drink.transform.parent = null;
	}

	public void CheckDrinkTypePrefab()
	{
		switch (SpawnerType)
		{
			case DrinkTypeSpawn.Milk:
				drinkPrefab = Resources.Load(Constants.MilkPrefabPath) as GameObject;
				break;
			case DrinkTypeSpawn.Pudding:
				drinkPrefab = Resources.Load(Constants.PuddingPrefabPath) as GameObject;
				break;
            case DrinkTypeSpawn.MilkFlip:
                drinkPrefab = Resources.Load(Constants.MilkFlipPrefabPath) as GameObject;
                break;
			default:
				break;
		}
	}

	private void CheckToSpawn()
	{
		if (!isFull)
		{
			SpawnDrink (drinkPrefab);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.MilkTag) || other.CompareTag (Constants.PuddingTag)) 
		{
			isFull = true;
			isStay = true;
		}	
	}

	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag (Constants.MilkTag) || other.CompareTag (Constants.PuddingTag))
			isStay = true;
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag (Constants.MilkTag) || other.CompareTag (Constants.PuddingTag))
			{
				isFull = false;
				isStay = false;
				StartCoroutine (CheckIsStay ());
			}
	}

	 IEnumerator CheckIsStay()
	{
		yield return new WaitForSeconds (1f);
		if (!isStay)
		{
			SpawnDrink (drinkPrefab);
		}
	}
	


}
